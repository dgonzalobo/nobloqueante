package es.dgonzalobo;

import java.nio.*; 
import java.nio.channels.*;
import java.net.*; 
import java.util.*;
import java.io.IOException;


public class miEchoServernb {
	public static int DEFAULT_PORT = 2002;
	public static int port;

	public static void main(String[] args) throws InterruptedException  { 
		try {
			port = Integer.parseInt(args[0]);
		}
		catch (Exception ex) { 
			port = DEFAULT_PORT; 
		}
		Byte numconnect=new Byte((byte)0);
		ServerSocketChannel serverChannel;
		Selector selector;
		try {
			System.out.println("\t___________Servidor No Bloqueante___________");
			System.out.println("\t               (PUERTO: "+port+")\n");
			serverChannel = ServerSocketChannel.open(); 
			ServerSocket ss = serverChannel.socket(); 
			InetSocketAddress address = new InetSocketAddress(port); 
			ss.bind(address);
			serverChannel.configureBlocking(false);
			selector = Selector.open();
			serverChannel.register(selector, SelectionKey.OP_ACCEPT);
		} 
		catch (IOException ex) { 
			ex.printStackTrace(); return;
		}
		while (true) { 
			try {
				selector.select();
			} 
			catch (IOException ex) {
				ex.printStackTrace();
				break; 
			}
			Set readyKeys = selector.selectedKeys(); 
			Iterator iterator = readyKeys.iterator(); 
			while (iterator.hasNext()) {
				Thread.sleep(5000);
				SelectionKey key=(SelectionKey) iterator.next(); 
				iterator.remove();
				try {
					if (key.isAcceptable()) {
						ServerSocketChannel server= (ServerSocketChannel) key.channel(); 
						SocketChannel client = server.accept();
						numconnect++;
						System.out.println("Accepted connection "+numconnect); 
						client.configureBlocking(false);
						SelectionKey clientKey = client.register(selector, SelectionKey.OP_WRITE | SelectionKey.OP_READ); 
						ByteBuffer buffer = ByteBuffer.allocate(100); clientKey.attach(buffer);
					}
					if (key.isReadable()) {
						SocketChannel client = (SocketChannel) key.channel(); 
						ByteBuffer output = (ByteBuffer) key.attachment(); 
						client.read(output);
					}
					if (key.isWritable()) {
						SocketChannel client = (SocketChannel) key.channel(); 
						ByteBuffer output = (ByteBuffer) key.attachment();
						if (output.position()>0) {
							byte[] b= numconnect.toString().getBytes(); 
							output.put (b,0,b.length); // o bien: output.put (b);
							output.put ((byte)'\r'); 
							output.put ((byte)'\n');
							output.flip();
							System.out.println("Mensaje: "+output);
							client.write(output);
							output.compact();
						} }
				}
				catch (IOException ex) {
					key.cancel();
					System.out.println("desactivo conexion, quedan "+--numconnect); 
					try {
						key.channel().close();
					} 
					catch (IOException cex) { 
						System.out.println("Exception: "+ cex);
					}
				}
			} // while iterator
		}// while true
	}	// main
}	// class miEchoServernb