package es.dgonzalobo;

import java.nio.*; 
import java.nio.channels.*;
import java.net.*; 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*; 
import java.io.IOException;


public class miEchoClientnb {
	public static int DEFAULT_PORT = 2002;

	public static void main(String[] args) throws InterruptedException { 
		String host= "localhost";
		if (args.length>0) 
			host=args[0];
		try {
			SocketAddress addr=new InetSocketAddress (host, DEFAULT_PORT); 
			SocketChannel client= SocketChannel.open(addr);
			ByteBuffer buffernet = ByteBuffer.allocate(100); //leer del servidor 
			WritableByteChannel out= Channels.newChannel(System.out); 
			
			client.configureBlocking(false); // socket no bloqueante 
			
			while (true) {
				Thread.sleep(5000);
				Date date = new Date();
				buffernet.flip();
				DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
				System.out.print(hourdateFormat.format(date)+" || Numero de Conexiones: "+client.read(buffernet)+"\n"); 
				buffernet.clear();
			}
		} 
		catch (IOException ex){ 
			ex.printStackTrace(); 
		} 
	} 
}


